package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import ru.stablex.ui.*;

class Main extends Sprite {
	
	public static function main() {
		// UIビルダーを初期化する。
		UIBuilder.init();
		
		// レイアウトファイルを読み込む。
		Lib.current.addChild(UIBuilder.buildFn('first.xml')());
	}
}
